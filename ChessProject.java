import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/**
 * <h1>Chess Game</h1>
 * The Chess Project implements an application that
 * simply allows the movement of chess pieces along a board, incorporating piece capturing
 *
 * @author  Stephen Kearns
 * @version 1.0
 * @since   2017-10-13
 *
 * @sources: https://www.youtube.com/watch?time_continue=1&v=WpC2UwDFefo
 */
 
public class ChessProject extends JFrame implements MouseListener, MouseMotionListener {
    JLayeredPane layeredPane;
    JPanel chessBoard;
    JLabel chessPiece;
	int xAdjustment;
	int yAdjustment;
	int landingX;
	int landingY;
	int startX;
	int startY;
	int xMovement;
	int yMovement;
	int initialX;
	int initialY;
	JPanel panels;
	JLabel pieces;
	Boolean validMove;
	private boolean inTheWay;
	private boolean success;
	int currentX;
	int currentY;
	int i;
	private String pieceName;
	private boolean whiteMove = true;

	public ChessProject(){
        Dimension boardSize = new Dimension(600, 600);
 
        //  Use a Layered Pane for this application
        layeredPane = new JLayeredPane();
        getContentPane().add(layeredPane);
        layeredPane.setPreferredSize(boardSize);
        layeredPane.addMouseListener(this);
        layeredPane.addMouseMotionListener(this);

        //Add a chess board to the Layered Pane 
        chessBoard = new JPanel();
        layeredPane.add(chessBoard, JLayeredPane.DEFAULT_LAYER);
        chessBoard.setLayout( new GridLayout(8, 8) );
        chessBoard.setPreferredSize( boardSize );
        chessBoard.setBounds(0, 0, boardSize.width, boardSize.height);
 
        for (int i = 0; i < 64; i++) {
            JPanel square = new JPanel( new BorderLayout() );
            chessBoard.add( square );
 
            int row = (i / 8) % 2;
            if (row == 0)
                square.setBackground( i % 2 == 0 ? Color.white : Color.gray );
            else
                square.setBackground( i % 2 == 0 ? Color.gray : Color.white );
        }
 
        // Setting up the Initial Chess board.
		for(int i=8;i < 16; i++){			
       		pieces = new JLabel( new ImageIcon("WhitePawn.png") );
			panels = (JPanel)chessBoard.getComponent(i);
	        panels.add(pieces);	        
		}
		pieces = new JLabel( new ImageIcon("WhiteRook.png") );
		panels = (JPanel)chessBoard.getComponent(0);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("WhiteKnight.png") );
		panels = (JPanel)chessBoard.getComponent(1);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("WhiteKnight.png") );
		panels = (JPanel)chessBoard.getComponent(6);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("WhiteBishop.png") );
		panels = (JPanel)chessBoard.getComponent(2);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("WhiteBishop.png") );
		panels = (JPanel)chessBoard.getComponent(5);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("WhiteKing.png") );
		panels = (JPanel)chessBoard.getComponent(3);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("WhiteQueen.png") );
		panels = (JPanel)chessBoard.getComponent(4);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("WhiteRook.png") );
		panels = (JPanel)chessBoard.getComponent(7);
	    panels.add(pieces);
		for(int i=48;i < 56; i++){			
       		pieces = new JLabel( new ImageIcon("BlackPawn.png") );
			panels = (JPanel)chessBoard.getComponent(i);
	        panels.add(pieces);	        
		}
		pieces = new JLabel( new ImageIcon("BlackRook.png") );
		panels = (JPanel)chessBoard.getComponent(56);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("BlackKnight.png") );
		panels = (JPanel)chessBoard.getComponent(57);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("BlackKnight.png") );
		panels = (JPanel)chessBoard.getComponent(62);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("BlackBishop.png") );
		panels = (JPanel)chessBoard.getComponent(58);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("BlackBishop.png") );
		panels = (JPanel)chessBoard.getComponent(61);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("BlackKing.png") );
		panels = (JPanel)chessBoard.getComponent(59);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("BlackQueen.png") );
		panels = (JPanel)chessBoard.getComponent(60);
	    panels.add(pieces);
		pieces = new JLabel( new ImageIcon("BlackRook.png") );
		panels = (JPanel)chessBoard.getComponent(63);
	    panels.add(pieces);		
    }

	private Boolean piecePresent(int x, int y){
		Component c = chessBoard.findComponentAt(x, y);

		return !(c instanceof JPanel);
	}

	/**
	 * Enables the taking of black opponents pieces
	 * <p>
	 * Checks the landing square, to identify if a opponents piece is present, if so true returns.
	 * <p>
	 * @param newX , this is the first parameter to CheckBlack and is the landing x coordinates.
	 * @param newX , this is the second parameter to CheckBlack and is the landing y coordinates.
	 * @return Opponent.

	 */
	private Boolean checkWhiteOpponent(int newX, int newY){
		Boolean opponent;
		Component c1 = chessBoard.findComponentAt(newX, newY);
		JLabel awaitingPiece = (JLabel)c1;
		String tmp1 = awaitingPiece.getIcon().toString();			
		if(((tmp1.contains("Black")))){
			if(tmp1.contains("King")){
				JOptionPane.showMessageDialog(null,"Game, over White wins");
				System.exit(0);
			}
			opponent = true;
		}
		else{
			opponent = false;
		}		
		return opponent;
	}


	/**
	 * Enables the taking of the White opponents pieces
	 * <p>
	 * Checks the landing square, to identify if a opponents piece is present, if present true is returns.
	 * <p>
	 * @param newX , this is the first parameter to CheckBlack and is the landing x coordinates.
	 * @param newX , this is the second parameter to CheckBlack and is the landing y coordinates.
	 * @return Opponent.

	 */
	private Boolean checkBlackOpponent(int newX, int newY){
		Boolean opponent;
		Component c1 = chessBoard.findComponentAt(newX, newY);
		JLabel awaitingPiece = (JLabel)c1;
		String tmp1 = awaitingPiece.getIcon().toString();
		if(((tmp1.contains("White")))){
			if(tmp1.contains("King")){
				JOptionPane.showMessageDialog(null,"Game, over Black wins");
				System.exit(0);
			}
			opponent = true;
		}
		else{
			opponent = false;
		}
		return opponent;
	}



	private Boolean checkKingLocation(int newX, int newY){
		boolean blocking = false;
		//check one sqaure to the left/right one above and behind
		//Also check one sqaure diagonally
		Component c = chessBoard.findComponentAt(newX,newY);
		if(c instanceof JPanel){
			blocking = false;
		}
		else{
			JLabel awaitingPiece = (JLabel) c;
			String piece = awaitingPiece.getIcon().toString();
			if(piece.contains("King")){
				blocking = true;
			}
		}

		return blocking;
	}

	/**
	 * Gets the name of the piece at a specific location
	 * <p>
	 * Finds the piece at a given location
	 * <p>
	 * @param newX , this is the first parameter to CheckBlack and is the landing x coordinates.
	 * @param newX , this is the second parameter to CheckBlack and is the landing y coordinates.
	 * @return piece, returns the pieces name

	 */

	private String CheckName(int newX, int newY){
		String piece = "";
		Component component = chessBoard.findComponentAt(newX, newY);
		if(component instanceof JLabel){
			JLabel awaitingPiece = (JLabel) component;
			piece = awaitingPiece.getIcon().toString();
			return piece;
		}
		return piece;
	}

	/**
	 * Replaces a piece
	 * <p>
	 *  If a piece is in the way of a move for example in front of a rook, the piece is replaced
	 * <p>
	 * @return Nothing

	 */
	private void ReplacePiece(){
		int location = 0;
		if (startY == 0) {
			location = startX;
		} else {
			location = (startY * 8) + startX;
		}
		String pieceLocation = pieceName + ".png";
		pieces = new JLabel(new ImageIcon(pieceLocation));
		panels = (JPanel) chessBoard.getComponent(location);
		panels.add(pieces);
	}

	/*
	    Ensures there is square space between the two of the kings, by firstly checking ahead to the squares around our landing position in the same fashion as the king's movements.
	*/

	/**
	 * Identifies if there is space, between the two kings
	 * <p>
	 *  Ensures there is square space between the two of the kings,
	 *  by firstly checking ahead to the squares around the landing position in the same fashion as the king's movements.
	 * <p>
	 * @return kingBlocking, returns true if a is within one square of the landing position.

	 */

	private boolean CheckKingPresentAt(int newX, int newY){
		boolean kingBlocking = false;

		if(piecePresent(newX, (newY+75)) && CheckName(newX, (newY+75)).contains("King") || piecePresent(newX, (newY-75)) && CheckName(newX, (newY-75)).contains("King")){
			kingBlocking = true;
		}else if(piecePresent((newX + 75), newY) && CheckName(newX +75, newY).contains("King") || piecePresent(newX -75, newY) && CheckName(newX - 75, newY).contains("King")){
			kingBlocking = true;
		}else if(piecePresent((newX + 75), (newY + 75)) && CheckName(newX + 75,newY + 75).contains("King") || piecePresent(newX - 75, newY-75) && CheckName(newX - 75, newY - 75).contains("King")){
			kingBlocking = true;
		}else {
			kingBlocking = piecePresent(newX + 75, newY - 75) && CheckName(newX + 75, newY - 75).contains("King") || piecePresent(newX - 75, newY + 75) && CheckName(newX - 75, newY + 75).contains("King");

		}
		return kingBlocking;
	}

	/*
		This method is called when we press the Mouse. So we need to find out what piece we have 
		selected. We may also not have selected a piece!
	*/
    public void mousePressed(MouseEvent e){
        chessPiece = null;
        Component c =  chessBoard.findComponentAt(e.getX(), e.getY());
        if (c instanceof JPanel) 
			return;
 
        Point parentLocation = c.getParent().getLocation();
        xAdjustment = parentLocation.x - e.getX();
        yAdjustment = parentLocation.y - e.getY();
        chessPiece = (JLabel)c;
		initialX = e.getX();
		initialY = e.getY();
		startX = (e.getX()/75);
		startY = (e.getY()/75);
        chessPiece.setLocation(e.getX() + xAdjustment, e.getY() + yAdjustment);
        chessPiece.setSize(chessPiece.getWidth(), chessPiece.getHeight());
        layeredPane.add(chessPiece, JLayeredPane.DRAG_LAYER);

    }
   
    public void mouseDragged(MouseEvent me) {
        if (chessPiece == null) return;
         chessPiece.setLocation(me.getX() + xAdjustment, me.getY() + yAdjustment);
     }
     
 	/*
		This method is used when the Mouse is released...we need to make sure the move was valid before 
		putting the piece back on the board.
	*/
    public void mouseReleased(MouseEvent e) {
		if (chessPiece == null) return;

		chessPiece.setVisible(false);
		success = false;
		success = false;
		inTheWay = false;
		Component c = chessBoard.findComponentAt(e.getX(), e.getY());
		String tmp = chessPiece.getIcon().toString();
		pieceName = tmp.substring(0, (tmp.length() - 4));
		validMove = false;
		landingX = e.getX() / 75;
		landingY = e.getY() / 75;
		currentX = e.getX();
		currentY = e.getY();
		xMovement = Math.abs(landingX - startX);
		yMovement = Math.abs((landingY - startY));


		System.out.println("--------------------------------------------------------------------");
		System.out.println("The Piece being moved is: " + pieceName);
		System.out.println("The starting coordinates is ( X: " + startX + ", Y: " + startY + ")");
		System.out.println("The X movement" + xMovement);
		System.out.println("The Y movement is " + yMovement);
		System.out.println("The landing coordinates are (" + landingX + " ," + landingY + ")");
		System.out.println("--------------------------------------------------------------------");


		/*d
			The only piece that has been enabled to move is a White Pawn...but we should really have this is a separate
			method somewhere...how would this work.
			
			So a Pawn is able to move two squares forward one its first go but only one square after that. 
			The Pawn is the only piece that cannot move backwards in chess...so be careful when committing 
			a pawn forward. A Pawn is able to take any of the opponent’s pieces but they have to be one
			square forward and one square over, i.e. in a diagonal direction from the Pawns original position.
			If a Pawn makes it to the top of the other side, the Pawn can turn into any other piece, for
			demonstration purposes the Pawn here turns into a Queen.
		*/
		if (pieceName.contains("King")) {
				KingMovement();
			}
			//TODO Refactor, reduce nesting, creating a abstract class for piece amd deriving the rest of the chess pieces
			else if (pieceName.contains("Queen")) {
				QueenMovement();
			} else if (pieceName.contains("Rook")) {
				RookMovement();
			} else if (pieceName.contains("Bishop")) {
			/*
			   The Bishop moves up the board in a diagonal direction
			 */
				BishopMovement();

			} else if (pieceName.contains("Knight")) {
				KnightMovement();
			} else if (pieceName.equals("WhitePawn")) {
				WhitePawnMovemnet();
			} // END OF WHITE PAWNY
			else if (pieceName.equals("BlackPawn")) {
				BlackPawnMovement();

			} // END OF THE BLACK PAWN

			if (!validMove) {
				ReplacePiece();
			} else {
				if (success) {
					//Get the promotional piece choice
					pieces = new JLabel(new ImageIcon(PromotionPieceChoice()));
					if (pieceName.contains("White")) {
						int location = 56 + (e.getX() / 75);
						if (c instanceof JLabel) {
							Container parent = c.getParent();
							parent.remove(0);
							//pieces = new JLabel(new ImageIcon("WhiteQueen.png"));
							parent = (JPanel) chessBoard.getComponent(location);
							parent.add(pieces);
						} else {
							Container parent = (Container) c;
							parent = (JPanel) chessBoard.getComponent(location);
							parent.add(pieces);
						}

					} else if (pieceName.contains("Black")) {
						int location = (e.getX() / 75);
						if (c instanceof JLabel) {
							Container parent = c.getParent();
							parent.remove(0);
							parent = (JPanel) chessBoard.getComponent(location);
							parent.add(pieces);
						} else {
							Container parent = (Container) c;
							parent = (JPanel) chessBoard.getComponent(location);
							parent.add(pieces);
						}


					}
					//Sets promoted piece visible and sets the overlapping piece false;
					chessPiece.setVisible(true);
					chessPiece.setVisible(true);
				} else {
					if (c instanceof JLabel) {
						Container parent = c.getParent();
						parent.remove(0);
						parent.add(chessPiece);
					} else {
						Container parent = (Container) c;
						parent.add(chessPiece);
					}
					chessPiece.setVisible(true);
				}
			}
	}
 
    public void mouseClicked(MouseEvent e) {
	
    }
    public void mouseMoved(MouseEvent e) {
   }
    public void mouseEntered(MouseEvent e){
	
    }
    public void mouseExited(MouseEvent e) {
	
    }

	/**
	 * Enables promotion to a number of different pieces
	 * <p>
	 *  Based on the selection of the user and the color the piece is assigned and return.
	 * <p>
	 * @Return promtionPiece, returns the full piece name.
	 */

    public String PromotionPieceChoice(){
		String[] choices = new String[]{"Queen", "Bishop", "Knight", "Rook"};
		String promtionPiece = pieceName.contains("White") ? "White":"Black";
		String favoritePizza = (String) JOptionPane.showInputDialog(null,
				"Choose a piece for promotion",
				"Promotion",
				JOptionPane.QUESTION_MESSAGE,
				null,
				choices,
				choices[0]);

		switch (favoritePizza.toLowerCase()){
			case "queen":
				promtionPiece = promtionPiece + "Queen.png";
				break;
			case "bishop":
				promtionPiece = promtionPiece + "Bishop.png";
				break;
			case "knight":
				promtionPiece = promtionPiece + "Knight.png";
				break;
			case "rook":
				promtionPiece = promtionPiece + "Rook.png";
				break;
			default: promtionPiece = "Invalid choice";
				break;
		}

		return promtionPiece;
	}


	/**
	 * Identifies if there is space, between the two kings
	 * <p>
	 * The king is restricted to moving one square at a time, the move can be in any direction i.e. Forward, Backwards, Diagonal
	 * When moving a king we must firstly check the surrounding squares, to determine if the opponents king is present,
	 * If the opponents king is present in the surrounding square of the move, and the gap between them is less than one square.
	 * Then the it is an invalid move.
	 * <p>
	 */

	public void KingMovement(){
		if (landingX < 0 || landingX > 7 || landingY < 0 || landingY > 7 || CheckKingPresentAt(currentX, currentY)) {
			validMove = false;
		}else{
			if (((xMovement == 1 && yMovement == 0) || (yMovement == 1 && xMovement == 0) || (xMovement == yMovement && xMovement == 1)) && ((pieceName.contains("White") && whiteMove) || (pieceName.contains("Black") && !whiteMove))) {
				if (!piecePresent(currentX, currentY)) {
					//if(pieceName.contains("White")) whiteMove = false;
					//else if(pieceName.contains("Black")) whiteMove = true;
					validMove = true;
				} else {
					if (piecePresent(currentX, currentY)) {
						if (pieceName.contains("White")) {
							if (checkWhiteOpponent(currentX, currentY)) {
								validMove = true;
								whiteMove = false;
							}
						} else {
							if (checkBlackOpponent(currentX, currentY)) {
								validMove = true;
								whiteMove = true;
							}
						}
					}
				}
			}
		}

		SwitchMove();

	}

	/**
	 * Controls the queen movement
	 * <p>
	 *  If the queen is moving in forward and backward, the rook movement is used
	 *  Else if the queen is moving in a diagonal direction the bishop movement is used
	 * <p>
	 */

	public void QueenMovement(){
		if (landingX < 0 || landingX > 7 || landingY < 0 || landingY > 7) {
			validMove = false;
		} else if((xMovement <= 7 && yMovement == 0) || (yMovement <= 7 &&  xMovement == 0)  && (((pieceName.contains("White") && whiteMove) || (pieceName.contains("Black") && !whiteMove))) && PieceHasMoved()) {
			RookMovement();
		}else if((xMovement == yMovement) && (((pieceName.contains("White") && whiteMove) || (pieceName.contains("Black") && !whiteMove))) && PieceHasMoved()){
			BishopMovement();
		}else {
			validMove = false;
		}
	}
	public void BishopMovement(){
			if (landingX < 0 || landingX > 7 || landingY < 0 || landingY > 7) {
				validMove = false;
			} else {
				if (xMovement == yMovement && (((pieceName.contains("White") && whiteMove) || (pieceName.contains("Black") && !whiteMove))) && PieceHasMoved()){
					validMove = true;
					if ((startX - landingX) < 0 && (startY - landingY) < 0) {
						for (i = 0; i < yMovement; i++) {
							if (piecePresent((initialX + (i * 75)), (initialY + (i * 75)))) {
								inTheWay = true;
							}
						}

					} else if (startX - landingX < 0 && startY - landingY > 0) {
						for (i = 0; i < yMovement; i++) {
							if (piecePresent((initialX + (i * 75)), (initialY - (i * 75)))) {
								inTheWay = true;
							}
						}

					} else if (startX - landingX > 0 && startY - landingY > 0) {
						for (i = 0; i < yMovement; i++) {
							if (piecePresent((initialX - (i * 75)), (initialY - (i * 75)))) {
								inTheWay = true;
							}
						}

					} else if (startX - landingX > 0 && startY - landingY < 0) {
						for (i = 0; i < yMovement; i++) {
							if (piecePresent((initialX - (i * 75)), (initialY + (i * 75)))) {
								inTheWay = true;
								break;
							}
						}

					}

					CapturePiece();
				} else {
					validMove = false;
				}

			}

			SwitchMove();
	}
	public void RookMovement(){
		if (landingX < 0 || landingX > 7 || landingY < 0 || landingY > 7) {
			validMove = false;
		} else if ((((xMovement <= 7 && yMovement == 0) || (yMovement <= 7 && xMovement == 0))) && (((pieceName.contains("White") && whiteMove) || (pieceName.contains("Black") && !whiteMove))) && PieceHasMoved()) {
			if (startX < landingX && yMovement == 0) {
				for (i = 0; i < xMovement; i++) {
					if (piecePresent((initialX + (i * 75)), initialY)) {
						inTheWay = true;
					}
				}
			} else if (startX > landingX && yMovement == 0) {
				for (i = 0; i < xMovement; i++) {
					if (piecePresent((initialX - (i * 75)), initialY)) {
						inTheWay = true;
					}
				}
			} else if (startY < landingY && xMovement == 0) {
				for (i = 0; i < yMovement; i++) {
					if (piecePresent((initialX), (initialY + (i * 75)))) {
						inTheWay = true;

					}
				}
			} else if (startY > landingY && xMovement == 0) {
				for (i = 0; i < yMovement; i++) {
					if (piecePresent((initialX), (initialY - (i * 75)))) {
						inTheWay = true;
					}
				}
			}
			CapturePiece();
		} else {
			validMove = false;
		}
		SwitchMove();
	}

	/**
	 * Method is used to a capture a piece
	 * <p>
	 *  Method to capture an opponents piece, the landing position of the board is firstly checked to see if piece is present,
	 	if so and the piece is of an opposing color, the piece is taken i.e. validMove.
	 * <p>
	 * @return Nothing
	 */

	private void CapturePiece() {
		if (inTheWay) {
            validMove = false;
        } else if(piecePresent(currentX, currentY)) {
            if (pieceName.contains("White")) {
                if (checkWhiteOpponent(currentX, currentY)) {
                    validMove = true;
                    whiteMove = false;
                } else {
                    validMove = false;
                }
            } else {
                if (checkBlackOpponent(currentX, currentY)) {
                    validMove = true;
                    whiteMove = true;
                } else {
                    validMove = false;
                }
            }
        }
        else{
            validMove = true;
        }
	}


	/**
	 * Method to check if a piece has moved
	 * <p>
	 *  Checks if piece has moved by comparing it's starting position and landing on the x-axis and y-axis
	 * <p>
	 * @return HasMoved, returns false if the piece has not moved
	 */
	public boolean PieceHasMoved(){
		boolean hasMoved = false;
		if(xMovement == 0 && yMovement == 0){
			return hasMoved;
		}
		else{
			hasMoved = true;
		}

		return hasMoved;
	}

	/**
	 * Method to switch the players turn
	 * <p>
	 * Switch's the players move to the negation of the current move
	 * <p>
	 * @return Nothing
	 *
	 */
	public void SwitchMove(){
		  if(validMove) {
			  whiteMove = !whiteMove;
		  }
	}


	/**
	 * Method controls the knight movement,
	 * <p>
	 *  A Knight's movement is restrained to either moving two squares in x-axis and one square in the y-axis or one square in the x-axis and two in the y-axis,
	 *  The knight can also jump pieces that are within its starting and landing position,
	 *  if an opponents piece is present at the knights landing position, it is taken.
	 *  Else the knight is unable to move as one of same color pieces are present in the location
	 * <p>
	 * @return Nothing
	 */

	public void KnightMovement(){
		if(landingX < 0 || landingX > 7 || landingY < 0 || landingY > 7 ){
			validMove = false;
		}
		else if (((xMovement == 1 && yMovement == 2) || (yMovement == 1 && xMovement == 2)) && ((pieceName.contains("White") && whiteMove) || (pieceName.contains("Black") && !whiteMove)) && PieceHasMoved()){
			if ((!piecePresent(currentX, currentY))) {
				/*if(pieceName.contains("White"))whiteMove = false;
				else if(pieceName.contains("Black")) whiteMove = true;*/
				validMove = true;

			} else {
				CapturePiece();
			}
		}else{
			validMove = false;
		}
		SwitchMove();
	}


	/*
	   ---------------------------------------------------------------

	   Tbe following methods control the White & Black pawns movement

	  ----------------------------------------------------------------
	 */

		/*
			So a Pawn is able to move two squares forward one its first go but only one square after that.
			The Pawn is the only piece that cannot move backwards in chess...so be careful when committing
			a pawn forward. A Pawn is able to take any of the opponent’s pieces but they have to be one
			square forward and one square over, i.e. in a diagonal direction from the Pawns original position.
			If a Pawn makes it to the top of the other side, the Pawn can turn into any other piece, for
			demonstration purposes the Pawn here turns into a Queen.
		*/


    public void WhitePawnMovemnet() {
		if (startY == 1) {
			if ((xMovement == 0) && (startY < landingY) && ((yMovement == 1) || (yMovement == 2)) && whiteMove) {
				if (yMovement == 2) {
					if (!piecePresent(currentX, currentY) && !piecePresent(currentX, (currentY - 75))) {
						validMove = true;
						whiteMove = false;
					} else {
						validMove = false;
					}
				} else {
					if (!piecePresent(currentX, currentY)) {
						validMove = true;
						whiteMove = false;
					} else {
						validMove = false;
					}
				}
			} else if ((xMovement == 1) && (yMovement == 1) && (startY < landingY) && whiteMove) {
				if ((piecePresent(currentX, currentY))) {
					if (checkWhiteOpponent(currentX, currentY)) {
						validMove = true;
						whiteMove = false;
					}

				} else {
					validMove = false;
				}
			} else {
				validMove = false;
			}
		} else {
			if ((xMovement == 0) && (yMovement == 1) && (startY < landingY) && whiteMove) {
				if (!piecePresent(currentX, currentY)) {
					if (landingY == 7) {
						//set value true for black knighting
						success = true;
						whiteMove = false;
					}
					validMove = true;
					whiteMove = false;
				}
			} else if (((xMovement == 1) && (yMovement == 1) && (startY < landingY)) && whiteMove) {
				if (piecePresent(currentX, currentY)) {
					if (checkWhiteOpponent(currentX, currentY)) {
						if (landingY == 7) {
							success = true;
							whiteMove = false;
						}
						validMove = true;
						whiteMove = false;
					}
				}
			}
		}
	}


	public void BlackPawnMovement(){
		if (startY == 6) {
			if (((xMovement == 0) && (startY > landingY) && ((yMovement == 1) || (yMovement == 2)))&& !whiteMove) {
				if (yMovement == 2) {
					if (!piecePresent(currentX, currentY) && !piecePresent(currentX, (currentY + 75))) {
						validMove = true;
						whiteMove = true;

					} else {
						validMove = false;
					}
				} else {
					if (!piecePresent(currentX, currentY)) {
						validMove = true;
						whiteMove = true;
					} else {
						validMove = false;
					}
				}
			} else if ((xMovement == 1) && (yMovement == 1) && (startY > landingY) && !whiteMove) {
				if ((piecePresent(currentX, currentY))) {
					if (checkBlackOpponent(currentX, currentY)) {
						validMove = true;
						whiteMove = true;
					}

				} else {
					validMove = true;
					whiteMove = true;
				}
			} else {
				validMove = false;
			}
		} else {
			if ((xMovement == 0) && (yMovement == 1) && (startY > landingY) && !whiteMove) {
				if (!piecePresent(currentX, currentY)) {
					if (landingY == 0) {
						//set value true for black knighting
						success = true;
						whiteMove = true;
					}
					validMove = true;
					whiteMove = true;
				}
			} else if (((xMovement == 1) && (yMovement == 1) && (startY > landingY)) && !whiteMove) {
				if (piecePresent(currentX, currentY)) {
					if (checkBlackOpponent(currentX, currentY)) {
						if (landingY == 0) {
							success = true;
							whiteMove = true;
						}
						validMove = true;
						whiteMove = true;
					}
				}
			}
		}
	}

}


