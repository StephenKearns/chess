import javax.swing.*;

public class PieceChoice {
    JLabel pieces;
    public JLabel GetChess(){
        String[] choices = new String[]{"Q", "B", "K", "R"};
        String favoritePizza = (String) JOptionPane.showInputDialog(null,
                "Choose a piece for promotion",
                "Promotion",
                JOptionPane.QUESTION_MESSAGE,
                null,
                choices,
                choices[0]);
        if(favoritePizza.equalsIgnoreCase("q")){
            pieces = new JLabel(new ImageIcon("WhiteQueen.png"));
        }else if(favoritePizza.equalsIgnoreCase("b")){
            pieces = new JLabel(new ImageIcon("WhiteBishop.png"));
        }else if(favoritePizza.equalsIgnoreCase("K")){
            pieces = new JLabel(new ImageIcon("WhiteKnight.png"));
        }else if(favoritePizza.equalsIgnoreCase("r")){
            pieces = new JLabel(new ImageIcon("WhiteRook.png"));
        }

        return pieces;
    }

}
